package test.claus.helloworld;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import test.claus.helloworld.vista.fragmentos.BienvenidaFragment;
import test.claus.helloworld.vista.fragmentos.SegundoFragment;

public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BienvenidaFragment.OnFragmentInteractionListener, SegundoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // Aparecer un fragment específico al iniciar la app
        this.onNavigationItemSelected(navigationView.getMenu().getItem(0));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sesiones.edit();
            editor.putBoolean("estaLogueado", false);
            editor.commit();

            Intent i = new Intent(HomeDrawerActivity.this, MainActivity.class);
            startActivity(i);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        android.support.v4.app.Fragment fragmentoActual = null;

        if (id == R.id.nav_inicio) {
            fragmentoActual = new BienvenidaFragment();
        } else if (id == R.id.nav_segundo) {
            fragmentoActual = new SegundoFragment();
        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_exit) {
            SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sesiones.edit();
            editor.putBoolean("estaLogueado", false);
            editor.commit();

            Intent i = new Intent(HomeDrawerActivity.this, MainActivity.class);
            startActivity(i);
            finish();
            return true;
        }

        if(fragmentoActual != null){
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.flContenedor, fragmentoActual).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String nFragmento, String nEvento) {
        if(nFragmento.equals("BienvenidaFragment")){
            if(nEvento.equals("BT_PRUBA_CLICK")){
                Toast.makeText(getApplicationContext(), "Hola desde el activitiy", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
