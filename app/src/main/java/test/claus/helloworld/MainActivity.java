package test.claus.helloworld;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.claus.helloworld.controlador.UsuariosController;

public class MainActivity extends AppCompatActivity {

    public TextView tvNombreUsuario, tvRegistrar;
    public EditText etNombreUsuario, etContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Validar si la persona ya había iniciado sesión
        SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
        boolean yaSeLogueo = sesiones.getBoolean("estaLogueado", false);
        if(yaSeLogueo){
            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }

        this.etNombreUsuario = findViewById(R.id.etNombreUsuario);
        this.etContrasena = findViewById(R.id.etContrasena);

        this.tvNombreUsuario = findViewById(R.id.tvNombreUsuario);
        this.tvRegistrar = findViewById(R.id.tvRegistrar);

        Button btIngresar = findViewById(R.id.btIngresar);
        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Capturar y mostrar los datos
                String nombreUsuario = etNombreUsuario.getText().toString().trim();
                String password = etContrasena.getText().toString().trim();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                int inicioSesion = controller.login(nombreUsuario, password);

                if(inicioSesion == 0){
                    // Inicio sesión
                    SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesiones.edit();
                    editor.putBoolean("estaLogueado", true);
                    editor.commit();

                    Intent intent = new Intent(MainActivity.this, HomeDrawerActivity.class);
                    startActivity(intent);
                    finish(); // Cerrar esta ventana
                }

                /* 1ra forma de mostrar el dato, de forma de permanente
                tvNombreUsuario.setText("Nombre Usuario: " + nombreUsuario);

                // 2da forma de mostrar el dato, de forma temporal, abre una ventana
                Toast.makeText(getApplicationContext(), "Usuario: " + nombreUsuario, Toast.LENGTH_LONG).show();

                // 3ra forma, muestra el dato al dev
                Log.w("PRUEBA ", "nombre usuario: " + nombreUsuario);*/

            }
        });

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent
                Intent intentNuevaVentana = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(intentNuevaVentana);
            }
        });
    }
}
