package test.claus.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import test.claus.helloworld.controlador.UsuariosController;

public class RegistrarUsuarioActivity extends AppCompatActivity {

    public EditText etUserName, etPass1, etPass2;
    public TextView tvMensaje;
    public Button btRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        etUserName = (EditText) findViewById(R.id.etUserName);
        etPass1 = (EditText) findViewById(R.id.etPass1);
        etPass2 = (EditText) findViewById(R.id.etPass2);

        tvMensaje = (TextView) findViewById(R.id.tvMensaje);

        btRegistrar = (Button) findViewById(R.id.btRegistrar);

        btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Capturar los datos
                String userName = etUserName.getText().toString().trim();
                String pass1 = etPass1.getText().toString().trim();
                String pass2 = etPass2.getText().toString().trim();

                UsuariosController capaController = new UsuariosController(getApplicationContext());
                try {
                    capaController.crearUsuario(userName, pass1, pass2);
                } catch (Exception ex){
                    String mensaje = ex.getMessage();
                    tvMensaje.setText(mensaje);
                }
            }

        });
    }
}
