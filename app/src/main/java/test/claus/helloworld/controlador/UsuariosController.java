package test.claus.helloworld.controlador;

import android.content.ContentValues;
import android.content.Context;

import test.claus.helloworld.modelo.MainDBContract;
import test.claus.helloworld.modelo.UsuariosModel;

public class UsuariosController {
    private UsuariosModel capaModelo;

    public UsuariosController(Context ctx){
        this.capaModelo = new UsuariosModel(ctx);
    }

    public void crearUsuario(String uName, String p1, String p2) throws Exception{
        // Validar los datos
        if(!p1.equals(p2)){
            // Pass no coinciden
            // Lanzar el Exception
            throw new Exception("Las contraseñas no coinciden.");
        }

        // Crear el usuario para enviarlo a la db
        ContentValues usuario = new ContentValues();
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, uName);
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, p1);

        this.capaModelo.crearUsuario(usuario);
    }

    public int login(String username, String password){
        // Devolvemos 0 cuando inició sesión
        // Cualquiera de los 2 datos falla, devuelve 1
        // Si el usuario no existe devuelve 2
        // Pedir el usuario a la db
        ContentValues usuario = this.capaModelo.obtenerUsuarioPorUsername(username);

        // Validamos que el usuario existe
        if(usuario == null){
            // El usuario no existe
            return 2;
        }

        // Validamos las passwords
        // Password guardada en la db
        String passDB = usuario.getAsString(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD);
        if(password.equals(passDB)){
            // Inicio exitoso
            return 0;
        }

        return 1;
    }
}
