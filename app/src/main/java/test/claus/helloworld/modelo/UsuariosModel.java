package test.claus.helloworld.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModel {
    private MainDBHelper dbHelper;

    public UsuariosModel(Context context){
        this.dbHelper = new MainDBHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(MainDBContract.MainDBUsuarios.NOMBRE_TABLA, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String username){
        // Abrir conexión
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // SELECT * FROM usuarios WHERE username = 'pepito' LIMIT 1

        // Projection
        String[] projection = {
                MainDBContract.MainDBUsuarios._ID,
                MainDBContract.MainDBUsuarios.COLUMNA_USERNAME,
                MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD
        };

        // Clausulas
        String selection = MainDBContract.MainDBUsuarios.COLUMNA_USERNAME + " = ? LIMIT 1";

        // Valores a buscar
        String[] selectionArgs = { username };

        Cursor cursor = db.query(
                MainDBContract.MainDBUsuarios.NOMBRE_TABLA,
                projection,
                selection,
                selectionArgs,
                null, null, null
        );

        if(cursor.moveToFirst()){
            // Significa que el usuario existe
            String username_cursor = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME));
            String password_cursor = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD));

            ContentValues usuario = new ContentValues();
            usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, username_cursor);
            usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, password_cursor);

            return usuario;
        }

        return null;
    }
}
